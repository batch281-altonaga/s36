// Controllers contain the functions and business logic

// Use require directive to access Task model from task.js
const Task = require("../models/task");

module.exports.getAllTasks = async (req, res) => {
  return Task.find().then((tasks) => {
    return tasks;
  });
};

module.exports.createTask = async (task) => {
  return Task.create(task).then((task) => {
    return task;
  });
};

module.exports.deleteTask = async (id) => {
  return Task.findByIdAndDelete(id)
    .then((removedTask) => removedTask)
    .catch((err) => {
      console.log(err);
      return false;
    });
};

module.exports.updateTask = async (id, task) => {
  return Task.findById(id).then((foundTask) => {
    if (!foundTask) {
      return false;
    }
    foundTask.name = task.name;
    foundTask.status = task.status;
    return foundTask.save().then((updatedTask) => {
      return updatedTask;
    });
  });
};
