// Contains all the endpoints for our application

const express = require("express");

// Creates a router instance that functions as a middleware and routing systems
const router = express.Router();

const taskController = require("../controllers/taskController");

/* Routes
 * - routes are responsible for handling requests and sending responses
 * - they invoke the controller functions
 * - all the business logic is in the controller functions
 */
router.get("/", (req, res) => {
  // Invoke getAllTasks function
  taskController.getAllTasks().then((tasks) => {
    res.send(tasks);
  });
});

router.post("/", (req, res) => {
  // Invoke createTask function
  taskController.createTask(req.body).then((task) => {
    res.send(task);
  });
});

router.put("/:id", (req, res) => {
  // Invoke updateTask function
  taskController.updateTask(req.params.id, req.body).then((task) => {
    res.send(task);
  });
});

router.delete("/:id", (req, res) => {
  // Invoke deleteTask function
  taskController.deleteTask(req.params.id).then((task) => {
    res.send(task);
  });
});

module.exports = router;
