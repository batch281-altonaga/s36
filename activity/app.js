// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

// Setup server
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Connect to the database
mongoose.connect(
  "mongodb+srv://twapegg:mEDXlo13wjUGhSal@wdc028-course-booking.5akuxcl.mongodb.net/",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: "s36-activity",
  }
);

// Set notification for connection success or failure
const db = mongoose.connection;

// If a connection error occurs, log the error message
db.on("error", console.error.bind(console, "connection error:"));

// If connection is successful, log a success message
db.once("open", () => {
  console.log("Connected to MongoDB database.");
});

// Add the task route
app.use("/tasks", taskRoute);

// Server listening
if (require.main === module) {
  app.listen(port, () => {
    console.log(`Server listening at http://localhost:${port}`);
  });
}

module.exports = app;
