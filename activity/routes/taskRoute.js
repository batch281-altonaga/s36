const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController");

// Routes

// Route for getting a specific task
router.get("/:id", (req, res) => {
  taskController.getSpecificTask(req, res).then((task) => {
    res.send(task);
  });
});

// Route for changing the status of a task to complete
router.put("/:id/complete", (req, res) => {
  taskController.completeTask(req, res).then((task) => {
    res.send(task);
  });
});

// Route for getting all tasks
router.get("/", (req, res) => {
  taskController.getAllTasks(req, res).then((tasks) => {
    res.send(tasks);
  });
});

// Route for creating a task
router.post("/", (req, res) => {
  taskController.createTask(req.body).then((task) => {
    res.send(task);
  });
});

module.exports = router;
