// Controllers contain the functions and business logic

// Use require directive to access Task model from task.js
const Task = require("../models/task");

// Create a controller function for retrieving a specific task
const getSpecificTask = async (req, res) => {
  return Task.findById(req.params.id).then((task) => {
    return task;
  });
};

// Create a controller function for changing the status of a task to complete
const completeTask = async (req, res) => {
  return Task.findById(req.params.id).then((task) => {
    task.status = "complete";
    return task.save().then((updatedTask) => {
      return updatedTask;
    });
  });
};

// Get all tasks
const getAllTasks = async (req, res) => {
  return Task.find().then((tasks) => {
    return tasks;
  });
};

// Create a task
const createTask = async (task) => {
  return Task.create(task).then((task) => {
    return task;
  });
};

// Export the controller functions
module.exports = {
  getSpecificTask,
  completeTask,
  getAllTasks,
  createTask,
};
